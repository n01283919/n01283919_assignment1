﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASSIGNMENT1
{
    public class Receipt
    {
        public Customers customers;
        public Services services;
        public BookingDetails bookingDetails;


        public Receipt(Customers c,Services s,BookingDetails bd)
        {
            customers = c;
            services = s;
            bookingDetails = bd;
        }

        public string PrintReceipt()
        {
            string receipt = "Your Receipt<br>";
            receipt += "Your Total is:" + calculatePrice().ToString() + "</br>";
            receipt += "First Name is:" +customers.customerFName +"</br>";
            receipt += "Last Name is:" +customers.customerLName +"</br>";
            receipt += "Phone Number:" + customers.customerPhNo + "</br>";
            receipt += "Email:" +customers.customerEmailID + "<br/>";

            receipt += "You Booked:" + bookingDetails.rooms + " Rooms <br/>";


            receipt += "Services selected is:" + string.Join(", ",services.services.ToArray())+ "</br>";
           
            return receipt;

        }
        public double calculatePrice()
        {
            double total = 0;

            if (bookingDetails.rooms == "Single")
            {
                total = 1000;
            }
            else if (bookingDetails.rooms == "Double")
            {
                total = 1500;
            }
           double sum = services.services.Count()*10;
            total += sum;
            return total;

        }
    }
}