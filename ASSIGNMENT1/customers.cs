﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASSIGNMENT1
{
    public class Customers
    {
        private string customerFirstName;
        private string customerLastName;
        private string customerPhone;
        private string customerEmail;

        public Customers()

        {
          
        }

        public string customerFName
        {
            get { return customerFirstName; }
            set {customerFirstName=value; }
        }

        public string customerLName
        {
            get { return customerLastName; }
            set { customerLastName = value; }
        }

        public string customerPhNo
        {
            get { return customerPhone; }
            set { customerPhone = value; }
        }

        public string customerEmailID
        {
            get { return customerEmail; }
            set { customerEmail = value; }
        }

    }
}