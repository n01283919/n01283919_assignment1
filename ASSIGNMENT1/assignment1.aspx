﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="assignment1.aspx.cs" Inherits="ASSIGNMENT1.assignment1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>HOTEL RESERVATION</title>
</head>
<body>
    <form id="form1" runat="server">
       
      
            <h1>HOTEL BOOKING RESERVATION</h1>
            <div id="bookings" runat="server">
            <asp:Label runat="server">First Name:</asp:Label>
            <asp:Textbox runat="server" ID="customerFirstName" placeholder="Your First Name" ></asp:Textbox>
            <asp:RequiredFieldValidator runat="server" ID="fN" ControlToValidate="customerFirstName" ErrorMessage="Enter Your First Name"></asp:RequiredFieldValidator>
            <br/>

            <asp:Label runat="server">Last Name:</asp:Label>
            <asp:Textbox runat="server" ID="customerLastName" placeholder="Your Last Name" ></asp:Textbox>
            <asp:RequiredFieldValidator runat="server" ID="lN" ControlToValidate="customerLastName" ErrorMessage="Enter Your Last Name"></asp:RequiredFieldValidator>
            <br/>

             <asp:Label runat="server">Gender:</asp:Label>
            <asp:RadioButton runat="server" ID="customergenderMale" Text="Male" GroupName="customergender"></asp:RadioButton>
            <asp:RadioButton runat="server" ID="customergenderFemale" Text="Female" GroupName="customergender"></asp:RadioButton>
            <br/>
            
             <asp:Label runat="server">E-mail ID:</asp:Label>
            <asp:Textbox runat="server" ID="customerEMail" placeholder="Your E-Mail ID" ></asp:Textbox>
            <asp:RequiredFieldValidator runat="server" ID="mailvalidator" ControlToValidate="customerEMail" ErrorMessage="Enter Your Mail"></asp:RequiredFieldValidator>
             <asp:RegularExpressionValidator runat="server" ID="customerailId" ControlToValidate="customerEMail" ErrorMessage="Please Enter Valid E-mail"  ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
             <br />

             <asp:Label runat="server">Phone Number:</asp:Label>
             <asp:Textbox runat="server" ID="customerPhoneNo" placeholder="Your Phone Number" OnTextChanged="PhoneNo_TextChanged" ></asp:Textbox>
             <asp:RequiredFieldValidator runat="server" ID="phoneValidator" ControlToValidate="customerPhoneNo" ErrorMessage="Enter Your Phone Number"></asp:RequiredFieldValidator>
             <asp:RegularExpressionValidator runat="server" ID="phno" ControlToValidate="customerPhoneNo" ErrorMessage="Please Enter Valid Phone Number"  ValidationExpression="[0-9]{10}"></asp:RegularExpressionValidator>
             <br/>

           <div>
            <asp:Label runat="server">Type of Rooms:</asp:Label>
           <asp:DropDownList runat="server" ID="roomType">
            <asp:ListItem runat="server">Single</asp:ListItem>
            <asp:ListItem runat="server">Double</asp:ListItem>
            </asp:DropDownList> </div>
           

            <div id="services_container" runat="server">
            <asp:Label runat="server">Services for you:</asp:Label>
            <asp:CheckBox runat="server" ID="Services1" Text="Massage"/>
            <asp:CheckBox runat="server" ID="Services2" Text="Spa"/>
            <asp:CheckBox runat="server" ID="Services3" Text="Room Cleaning"/>
            <asp:CheckBox runat="server" ID="Services4" Text="Rental Car"/>
            <asp:CheckBox runat="server" ID="Services5" Text="Breakfast"/>
            </div>

            <asp:Button runat="server" Text="Submit" ID="myButton" OnClick="Booking"/>

            <asp:ValidationSummary runat="server" ID="summary" HeaderText="You Must Enter Following Fields:"/>

            <div runat="server" id="Details"/>
         
</div>        
</form>
</body>
</html>
