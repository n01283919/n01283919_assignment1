﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ASSIGNMENT1
{
    public partial class assignment1 : System.Web.UI.Page
    {
        private object services_bucket;

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void CheckBoxList1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void CheckBox1_CheckedChanged(object sender, EventArgs e)
        {

        }

        protected void PhoneNo_TextChanged(object sender, EventArgs e)
        {

        }

        protected void CheckBox1_CheckedChanged1(object sender, EventArgs e)
        {

        }

        protected void RadioButtonList1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        public void Booking(object sender, EventArgs e)
        {
            if (!Page.IsValid)
            {
                return;
            }
           // bookings.InnerHtml = "Thank you for your booking!";

            //customer details

            string fname = customerFirstName.Text.ToString();
            string lname = customerLastName.Text.ToString();
            string email = customerEMail.Text.ToString();
            string phone = customerPhoneNo.Text.ToString();
            Customers newcustomers = new Customers();
            newcustomers.customerFName= fname;
            newcustomers.customerLName= lname;
            newcustomers.customerEmailID= email;
            newcustomers.customerPhNo= phone;

            //services

            List<string> services = new List<string> { };
            Services newservices = new Services(services);

            //booking details

            string room = roomType.SelectedItem.Value.ToString();
            BookingDetails newBookingDetails = new BookingDetails(room);


            //adding services
            List<string> extraservices = new List<string>();

            foreach(Control control in services_container.Controls)
            {
                if(control.GetType()==typeof(CheckBox))
                {
                    CheckBox service = (CheckBox)control;
                    if(service.Checked)
                    {
                        extraservices.Add(service.Text);
                    }
                }
            }

            newservices.services = newservices.services.Concat(extraservices).ToList();

            Receipt newbookings = new Receipt(newcustomers, newservices,newBookingDetails);

            Details.InnerHtml = newbookings.PrintReceipt();
        }
       
    }
}